CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The taxonomy_image module allows site administrators to associate images with
taxonomy terms.

The module allows the admin to create a one-to-one term-to-image relationship.
With image display recursion, it is also possible to create a many-to-one
relationship. This is useful if creating taxonomy hierarchies in which an
entire tree of terms will use the same image. With recursion enabled, you
only need to associate an image with the tree parent, and all children will
automatically inherit the same image.

REQUIREMENTS
------------
This module requires the following modules:

 * Taxonomy
 * Image

INSTALLATION
------------

Install as normal (see http://drupal.org/documentation/install/modules-themes).

CONFIGURATION
-------------

In manage form display of taxonomy display image field from disabled to enabled.
As by default it is kept hidden/disabled.

MAINTAINERS
-----------
QED42
nehajyoti
